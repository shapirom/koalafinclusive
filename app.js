// Require the Bolt package (github.com/slackapi/bolt)
const { App, ExpressReceiver } = require("@slack/bolt");

// Create a Bolt Receiver
const receiver = new ExpressReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
});

const terms = require("./data/inclusive.json");

const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  receiver,
});

const shouldFilter = (text, phrase) => {
  if (Array.isArray(phrase)) {
    return phrase.filter((p) => matches(text, p)).length > 0;
  } else {
    return matches(text, phrase);
  }
};

const matches = (text, phrase) => {
  // console.log(`${text}: ${phrase} - ${text.toLowerCase(phrase)}`)
  return text === null || text === undefined
    ? false
    : text.toLowerCase().includes(phrase);
};

const getReplacement = (sub) => {
  if (Array.isArray(sub)) {
    return sub[Math.floor(Math.random() * sub.length)];
  } else {
    return sub;
  }
};

const buildMessage = (matched, user, channel) => {
  const suggestion = getReplacement(matched.replace);

  // Call chat.postMessage with the built-in client
  return {
    channel: channel,
    user: user,
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `Hello there. I detected you used the term \`${matched.phrase}\``,
        },
      },
      {
        type: "divider",
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `${suggestion}`,
        },
      },
    ],
    text: `Hello, I saw you said ${matched.phrase}\n${suggestion}`,
    // thread_ts: event.ts
  };
};

app.event("message", async ({ event, client, context }) => {
  //console.log(event);

  const matched = terms.filter((t) => shouldFilter(event.text, t.phrase));
  // console.log(matched)
  if (matched.length > 0) {
    try {
      const result = await client.chat.postEphemeral(
        buildMessage(matched[0], event.user, event.channel)
      );
      // console.log(result);
    } catch (error) {
      console.error(error);
    }
  }

  // console.log(matched);
});

// All the room in the world for your code
app.event("app_home_opened", async ({ event, client, context }) => {
  try {
    /* view.publish is the method that your app uses to push a view to the Home tab */
    const result = await client.views.publish({
      /* the user that opened your app's app home */
      user_id: event.user,

      /* the view object that appears in the app home*/
      view: {
        type: "home",
        callback_id: "home_view",

        /* body of the view */
        blocks: [
          {
            type: "section",
            text: {
              type: "mrkdwn",
              text: "*Welcome to Koalafinclusive* :koalafi-logo: :koala:",
            },
          },
          {
            type: "divider",
          },
          {
            type: "section",
            text: {
              type: "mrkdwn",
              text: "This app is intended to raise awareness of inclusive terms and notify you if you've used something that others may find offensive or disturbing.",
            },
          },
        ],
      },
    });
  } catch (error) {
    console.error(error);
  }
});

receiver.router.get("/healthcheck", (req, res) => {
  console.log(Date.now() + " Ping Received");
  res.sendStatus(200);
});

const http = require("http");

(async () => {
  // Start your app
  await app.start(process.env.PORT || 3000);

  setInterval(() => {
    const project = `http://${process.env.PROJECT_DOMAIN}.glitch.me/healthcheck`;

    var opts = require("url").parse(project);
    opts.headers = {
      "User-Agent": "javascript",
    };

    const req = http.get(opts, (res) => {
      console.log(`statusCode: ${res.statusCode}`);

      res.on("data", (d) => {
        process.stdout.write(d);
      });
    });

    req.on("error", (error) => {
      console.error(error);
    });

    req.end();
  }, 280000);

  console.log("⚡️ Bolt app is running!");
})();
